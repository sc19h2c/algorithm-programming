import random
import csv

def createNum(max_size, max_value, num_items):
    list_items = []
    for i in range(num_items):
        size = random.randint(1,max_size)
        value = random.randint(1,max_value)
        list_items.append((i,size,value))
    return list_items

if __name__ == "__main__":
    max_size = int(input("Enter max size :"))
    max_value = int(input("Enter max value :"))
    num_items = int(input("Enter number of items :"))
    list_items = createNum(max_size, max_value, num_items)
    with open("knapsack_data.csv","w") as f:
        f.write("item_number,size,value\n")
        for item in list_items:
            f.write(str(item[0]) + "," + str(item[1]) + "," + str(item[2]) + "\n")
