def knapsack(items, capacity):
    n = len(items)
    table = [[0 for _ in range(capacity + 1)] for _ in range(n + 1)]

    for i in range(1, n + 1):
        for j in range(capacity + 1):
            if items[i - 1][1] > j:
                table[i][j] = table[i - 1][j]
            else:
                table[i][j] = max(table[i - 1][j], table[i - 1][j - items[i - 1][1]] + items[i - 1][2])

    selected_items = []
    i = n
    j = capacity
    while i > 0 and j > 0:
        if table[i][j] != table[i - 1][j]:
            selected_items.append(items[i - 1][0])
            j -= items[i - 1][1]
        i -= 1
    return selected_items