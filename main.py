import csv
from multi_bin_knapsack import knapsack as multi_bin_knapsack
from item_centric_knapsack import knapsack as item_centric_knapsack
import time


def read_data(filename):
    data = []
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data.append((int(row['item_number']), int(row['size']), int(row['value'])))
    return data


if __name__ == "__main__":
    capacity = int(input("Enter capacity of knapsack :"))
    items = read_data("knapsack_data.csv")
    #Record time
    mb_start_time = time.time()
    multi_bin_items = multi_bin_knapsack(items, capacity)
    mb_end_time = time.time()
    print("Multi-Bin Knapsack :")
    print("Number of items :", len(multi_bin_items))
    print(multi_bin_items)
    print("Sum of values :", sum([items[i][2] for i in multi_bin_items]))
    print("Time cost :", mb_end_time - mb_start_time)

    ic_begin_time = time.time()
    item_centric_items = item_centric_knapsack(items, capacity)
    ic_end_time = time.time()
    print("Item-Centric Knapsack :")
    print("Number of items :", len(item_centric_items))
    print(item_centric_items)
    print("Sum of values :", sum([items[i][2] for i in item_centric_items]))
    print("Time cost :", ic_end_time - ic_begin_time)