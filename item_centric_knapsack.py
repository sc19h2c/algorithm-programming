def knapsack(items, capacity):
    n = len(items)
    items.sort(key=lambda x: x[2] / x[1], reverse=True)

    selected_items = []
    i = 0
    while i < n and capacity > 0:
        if items[i][1] <= capacity:
            selected_items.append(items[i][0])
            capacity -= items[i][1]
        i += 1
    return selected_items